Hikari documentation
####################

This is for version |version|

Packages and submodules
-----------------------

.. autosummary::
    :toctree: modules

    hikari
    hikari.compat
    hikari.errors
    hikari.models.audit.action_type
    hikari.net.basic_bot
    hikari.net.debug
    hikari.net.gateway
    hikari.net.http
    hikari.net.http.base
    hikari.net.http.client
    hikari.net.opcodes
    hikari.net.rates
    hikari.net.status

* `Changelog, code quality statistics and reports for this release <quality_assurance.html>`_
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
