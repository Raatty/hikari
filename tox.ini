[tox]
envlist =
    py36
    py37
    py38
    # py39
    pypy3
    bandit
    black-check
    report
    docs

isolated_build = true
skip_missing_interpreters = true

[testenv]
description = run the tests with pytest under {basepython}
setenv =
    COVERAGE_FILE = public/coverage-{envname}
passenv =
    http_proxy https_proxy no_proxy SSL_CERT_FILE PYTEST_* RUN_SLOW_TESTS
extras = testing
commands =
    python -c "__import__('os').makedirs('public',exist_ok=True)"
    pytest --cov "hikari" --cov-config "{toxinidir}/tox.ini" --cov-report=term --cov-branch --testdox {posargs:.} hikari_tests/
    coverage xml -o public/coverage.{envname}.xml

[pytest]
addopts = -ra --showlocals
testpaths = tests
xfail_strict = True

[coverage:paths]
source = hikari

[coverage:run]
branch = true
parallel = false
omit =
    # Should contain no useful logic.
    hikari/__init__.py
    # System specific. Other tests will fail if these don't work.
    hikari/compat/*
timid = false

[coverage:report]
skip_covered = False
show_missing = True
precision = 1
exclude_lines =
    \#\s*pragma: no cover
    ^\s*raise AssertionError\b
    ^\s*raise NotImplementedError\b
    ^\s*return NotImplemented\b
    ^\s*raise$
    ^\s*pass$
    ^if __name__ == ['"]__main__['"]:$

[testenv:report]
skip_install = true
description =
    create several reports from test running and other analytics and dump them in the public directory.
    This will require sh, grep, and find.
deps =
    diff-cover==2.1.0
    coverage==4.5.3
whitelist_externals =
    sh
commands =
    sh .ci/aggregate-coverage.sh
    sh .ci/changelog.sh

[testenv:docs]
description = invoke sphinx-build to build the HTML docs
deps =
    sphinx==2.0.1
    sphinx-autodoc-typehints==1.6.0
    sphinxcontrib-asyncio==0.2.0
    sphinx-bootstrap-theme==0.7.1
setenv =
    PAPER = a4
    SPHINXOPTS = -WTvvn
commands =
    sphinx-apidoc -o docs/modules -e --implicit-namespaces -M hikari
    sphinx-build docs public -b html
    # sphinx-build docs public/tex -b latex

[testenv:bandit]
description = invoke static application security testing with bandit
deps = 
    bandit==1.6.0
commands =
    bandit --ini=tox.ini -r

[testenv:bandit-report]
description = produces an HTML report of findings by bandit
deps =
    bandit==1.6.0
commands =
    bandit --ini=tox.ini -f html -o public/bandit.html -r

[bandit]
targets = hikari
recursive = true

[testenv:black-check]
skip_install = true
description = check the source code formatting. Run `tox -e black-fix` to automatically fix any issues.
deps =
    black
whitelist_externals =
    python
commands =
    python .ci/black.py hikari --check

[testenv:black-fix]
skip_install = true
description = reformat any source code
deps =
    black
whitelist_externals =
    python
commands =
    python .ci/black.py hikari hikari_tests setup.py docs/conf.py
