#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A lightweight, flexible, customizable Discord API wrapper for Python.
"""
__all__ = ("__author__", "__contributors__", "__copyright__", "__license__", "__version__", "__url__")

__author__ = "Nekokatt"
__contributors__ = {"LunarCoffee", "raatty", "Tmpod"}
__copyright__ = f"© {__import__('datetime').datetime.now().year} {__author__}"
__license__ = "zlib"
__version__ = "0.0.1-SNAPSHOT"
__url__ = "https://gitlab.com/nekokatt/hikari"
