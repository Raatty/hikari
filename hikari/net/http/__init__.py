#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
HTTP component implementations for the V7 Discord HTTP gateway.
"""
from .client import HTTPClient
